
data User = User {
    nom :: String,
    prenom :: String,
    age :: Int
}

showUser :: User -> String
-- showUser (User nom prenom age) = nom ++ " " ++ prenom ++ " " ++ (show age)
showUser u = nom u ++ " " ++ prenom u ++ " " ++ (show (age u))

incAge :: User -> User
-- incAge (User nom prenom age) = (User nom prenom (age+1))
incAge u = u { age = age u + 1}

main :: IO ()
main = do
    let user1 = User { nom = "DELSART", prenom = "Maxime", age = 20}
    let user2 = User "doe" "John" 42

    print (showUser user1)
    print (showUser (incAge user1))

