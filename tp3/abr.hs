import Data.List (foldl')

-- TODO Abr
data Tree = Leaf | Node Int Tree Tree

-- insererAbr 
insererAbr :: Int -> Tree -> Tree
insererAbr x Leaf = Node x Leaf Leaf
insererAbr x (Node y left right)
    | x <= y = Node y (insererAbr x left) right
    | otherwise = Node y left (insererAbr x right)


-- listToAbr 
listToAbr :: [Int] -> Tree
listToAbr = foldr insererAbr Leaf

-- abrToList 
abrToList :: Tree -> [Int]
abrToList Leaf = []
abrToList (Node x l r) =  abrToList l ++ [x] ++ abrToList r

main :: IO ()
main = do
    let t1 = Leaf
        t2 = Node 12 (Node 1 Leaf (Node 70 Leaf Leaf)) (Node 2 (Node 16 Leaf Leaf) (Node 48 Leaf Leaf))
        t3 = insererAbr 65 t1
        t4 = insererAbr 3 t2
        t5 = listToAbr [12, 3, 45, 12, 488, 7]

    print "TODO"