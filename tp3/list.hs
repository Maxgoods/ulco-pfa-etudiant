data List a = Nil | Cons a (List a)

-- sumList 
sumList :: List Int -> Int
sumList Nil = 0
sumList (Cons x xs) = x + (sumList xs)

-- flatList 

flatList :: List String -> String
flatList Nil = ""
flatList (Cons elm tail) = elm ++ (flatList tail)

-- toHaskell 
toHaskell :: List a -> [a]
toHaskell Nil = []
toHaskell (Cons x xs) = x : toHaskell xs

-- fromHaskell 
fromHaskell :: [a] -> List a 
-- fromHaskell [] = Nil
-- fromHaskell (x:xs) = Cons x (fromHaskell xs)
fromHaskell = foldr Cons Nil

-- myShowList
myShowList :: Show a => List a -> String
-- myShowList Nil = ""
-- myShowList (Cons elm tail) = show elm ++ " " ++ myShowList tail
myShowList xs = unwords (map show (toHaskell xs))

main :: IO ()
main = do 
    let l1 = Cons 2 Nil
        l2 = Cons 2 (Cons 3 Nil)

    print (sumList l2)
    print (toHaskell l2)
    print (toHaskell (fromHaskell [1..4]))
    print (myShowList l2)

