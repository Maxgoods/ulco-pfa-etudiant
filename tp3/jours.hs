
data Jour = Lundi | Mardi | Mercredi | Jeudi | Vendredi | Samedi | Dimanche

estWeekend :: Jour -> Bool
-- estWeekend Samedi || Dimanche = True
estWeekend Samedi = True
estWeekend Dimanche = True
estWeekend _ = False


compterOuvrables :: [Jour] -> Int
-- compterOuvrables xs = length [x | x <- xs, estWeekend x] 
compterOuvrables = length . filter (not . estWeekend)

compterOuvrables' :: [Jour] -> Int
compterOuvrables' [] = 0
compterOuvrables' (x:xs) = 
    if estWeekend x == False
        then compterOuvrables' xs +1
        else compterOuvrables' xs

main :: IO ()
main = do
    let j1 = Lundi
        j2 = Samedi
    
    print (estWeekend j1)
    print (compterOuvrables' [j1, j2])
    print (compterOuvrables [j1, j2])


