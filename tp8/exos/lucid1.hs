{-# LANGUAGE OverloadedStrings #-}

import Lucid
import Data.Text.Lazy.IO as L

myhtml :: Html ()
myhtml = do
    h1_ "hello"
    p_ "word"
	
myhtml2 :: Html ()
myhtml2 = do
    h1_ "hello" 
    div_ $ do
        p_ "dfs"
        h2_ "ldkjs"
    p_ "word"

myhtml2' :: Html ()
myhtml2'
    = h1_ "hello" 
    >> div_ 
        ( p_ "dfs"
        >> h2_ "ldkjs")
    >> p_ "word"


main :: IO
main = L.putStrLn $ renderText myhtml