{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import Data.Aeson

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: Int
    } deriving (Show)

instance ToJSON Person
    toJSON (Person firstname lastname birthdate) =
        object ["first" .: name, "last" .: lastname, "birth" .: birthdate ]

persons :: [Person]
persons =
    [ Person "John" "Doe" 1970
    , Person "Haskell" "Curry" 1900
]

main :: IO ()
main = do
	encodeFile "out-aeson2.json" persons
