{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module Site where

import Data.Aeson
import GHC.Generics ( Generic )
import Lucid

data Site = Site
    {imgs   :: [String]
    , url   :: String
    } deriving (Generic, Show)

instance FromJSON Site

-- TODO afficher les images
createHtmlFile :: Site -> Html ()
createHtmlFile oneSite = do
   doctype_
   html_ $ do
       head_ $ meta_ [charset_ "utf8"]
       body_ $ do
            h1_ "test"

createSites :: [Site] -> IO ()
createSites [] = print "fini bbb"
createSites (x : xs) = do
    let filePatha = "./output/" ++ show (length xs) ++ ".html"
    renderToFile filePatha (createHtmlFile x)
    createSites xs

loadSites :: String -> IO (Either String [Site])
loadSites = eitherDecodeFileStrict
