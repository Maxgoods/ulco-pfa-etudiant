{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module Album where

import Lucid
import Site
import Data.Text as T

mkPage :: [Site] -> Html ()
mkPage sites = do
   doctype_
   html_ $ do
       head_ $ meta_ [charset_ "utf8"]
       body_ $ do
            h1_ "Album"
            mapM_ mkSite sites

mkSite :: Site -> Html ()
mkSite s = div_ $ do
    h2_ $ toHtml $ url s
    mapM_ mkImg (imgs s)

mkImg :: String -> Html ()
mkImg i = img_ [src_ (T.pack i)]

genAlbum :: String -> [Site] ->  IO ()
genAlbum filename sites = renderToFile filename (mkPage sites)