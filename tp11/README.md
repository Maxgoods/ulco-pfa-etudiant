# tp11

what: app
how: nix + cabal + ghcide

author: Maxime Delsart

# RUN
- nix-shell
- sqlite3 tp11.db < tp11.sql
- cabal build
- cabal run