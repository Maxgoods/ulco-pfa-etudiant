{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module View where

import Model
import Lucid
import Data.Text as T
import Data.Text.Lazy as L
import TextShow

bootstrap :: T.Text
bootstrap = "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"

mkPage :: L.Text -> Html () -> L.Text
mkPage myTitle myHtml = renderText $ do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            meta_ [ name_ "viewport"
                  , content_ "width=device-width,initial-scale=1,shrink-to-fit=no"]
            link_ [ rel_ "stylesheet", href_ bootstrap]
            link_ [ rel_ "stylesheet", href_ "style.css"]
            title_ $ toHtml myTitle
        body_ $ div_ [class_ "container"] myHtml



viewIndexPage :: L.Text
viewIndexPage = mkPage "ulco forum - Delsart Maxime" $ do
    h1_ $  a_ [href_ "/"] "Ulcoforum"
    a_ [href_ "/allData"] "AllData"
    span_ " - "
    a_ [href_ "/allThreads"] "allThreads"
    p_ "This is ulcoForum"

viewAllData :: [UserMessage] -> L.Text
viewAllData allData = mkPage "viewAllData - ulco forum - Delsart Maxime" $ do
    h1_ "All messages :"
    ul_ $ do
         mapM_ viewOneData allData
   

viewOneData :: UserMessage -> Html ()
viewOneData (UserMessage username text threadTitle)  = div_ [class_ "row oneData"] $ do
    h1_ [class_ "col-md-12"] $ toHtml $ threadTitle
    p_ $ toHtml $ showt username <> " : " <> showt text

viewAllThreads :: [Threads] -> L.Text
viewAllThreads allThreads = mkPage "viewAllThreads - ulco forum - Delsart Maxime" $ do
    h1_ "All threads :"
    ul_ $ do
         mapM_ viewOneThread allThreads

viewOneThread :: Threads -> Html ()
viewOneThread (Threads id title) = div_ [class_ "row oneData"] $ do
    -- li_ $ a_ [href_ "/oneThread/"] $ title
    p_ $ toHtml $ showt title