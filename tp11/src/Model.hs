{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module Model where

import Database.SQLite.Simple
import Data.Text (Text)
import GHC.Generics (Generic)

data UserMessage = UserMessage 
    { _username    :: Text
    , _text :: Text
    ,  _thread_title :: Text
    } deriving (Generic, Show)

instance FromRow UserMessage where
    fromRow = UserMessage <$> field <*> field <*> field

data Threads = Threads 
    { _id    :: Int
    , _title :: Text
    } deriving (Generic, Show)

instance FromRow Threads where
    fromRow = Threads <$> field <*> field

dbSelectAllUserMessages :: Connection -> IO [UserMessage]
dbSelectAllUserMessages conn = 
    query_ conn "SELECT userMessage_username, userMessage_text,\
    \thread_title \
    \FROM userMessage \
    \INNER JOIN thread ON userMessage_thread_id = thread_id"

dbSelectAllThreads :: Connection -> IO [Threads]
dbSelectAllThreads conn = 
    query_ conn "SELECT thread_id, thread_title \
    \FROM thread"

--dbSelectAllUserMessagesFromThreadId :: Connection -> Int -> IO [[Int, Text, Text, Text, Int]]
--dbSelectAllUserMessagesFromThreadId conn i = 
--    query conn "SELECT userMessage_id, userMessage_username, userMessage_text,\
--    \thread_title, userMessage_thread_id \
 --   \FROM userMessage \
   -- \INNER JOIN thread ON userMessage_thread_id = thread_id \
   -- \WHERE thread_id = ?" (Only i)