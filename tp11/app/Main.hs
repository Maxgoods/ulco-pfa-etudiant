{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import Network.Wai.Middleware.Static (staticPolicy, addBase)
import Network.Wai.Middleware.Gzip (gzip, def, gzipFiles, GzipFiles(..))
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Web.Scotty
import Control.Monad.IO.Class (liftIO)
import Database.SQLite.Simple
import qualified Data.Text.Lazy as L
import TextShow
import GHC.Generics

import Model
import View

main :: IO ()
main = scotty 3000 $ do
    middleware logStdoutDev
    middleware $ gzip def { gzipFiles = GzipCompress }
    middleware $ staticPolicy $ addBase "static"
    
    get "/" $ html $ viewIndexPage
    get "/allData" $ do
        allData <- liftIO $ withConnection "tp11.db" Model.dbSelectAllUserMessages
        html $ viewAllData allData
    get "/allThreads" $ do
            allThreads <- liftIO $ withConnection "tp11.db" Model.dbSelectAllThreads
            html $ viewAllThreads allThreads
    --get "/oneThread/:threadId/" $ do
        --threadId <- showt <$> param "threadId"
        --html $ viewOneThread
