CREATE TABLE thread (
  thread_id INTEGER PRIMARY KEY AUTOINCREMENT,
  thread_title TEXT
);

CREATE TABLE userMessage (
  userMessage_id INTEGER PRIMARY KEY AUTOINCREMENT,
  userMessage_username TEXT,
  userMessage_text TEXT,
  userMessage_thread_id INTEGER,
  FOREIGN KEY(userMessage_thread_id) REFERENCES thread(thread_id)
);

INSERT INTO thread VALUES(1, 'Vacances 2020-2021');
INSERT INTO thread VALUES(2, 'Master Info - Nouveau forum');

INSERT INTO userMessage VALUES(1, "Maxime", "ca existe les vacances ?", 1);
INSERT INTO userMessage VALUES(2, "Tata", "Tu sais, le confinement, ca existe", 1);
INSERT INTO userMessage VALUES(3, "Maxime", "Le teletravail aussi", 1);
INSERT INTO userMessage VALUES(4, "Maxime", "Bienvenue sur le nouveau forum !", 2);
