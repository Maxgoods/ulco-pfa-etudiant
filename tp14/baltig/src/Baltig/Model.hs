{-# LANGUAGE OverloadedStrings #-}

module Baltig.Model where

import Miso.String
import Network.URI (URI)

import Baltig.News

import qualified Database.Selda as SQL

data Model = Model
    { uri           :: URI
    , news          :: [News]
    } deriving (Eq)

createModel :: URI -> Model
createModel myUri = Model myUri 
        [News (SQL.toId 42) "title" "date" "body"
        , News (SQL.toId 43) "autre titre" "autre date" "autre body"
        ]

