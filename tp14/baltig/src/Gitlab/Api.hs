{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}

module Gitlab.Api where 

import Data.Proxy (Proxy(..))
import Data.Text
import Network.URI (URI, URIAuth(..))
import Servant.API
import Servant.Links

import Gitlab.User

type UserApi = "api" :> "v4" :> "users" :> QueryParam' ' [Required] "username" Text :> Get '[JSON] [User]
type GitlabApi = UserApi

userRoute :: URI
userRoute = toRemote $ linkURI $ safeLink (Proxy @GitlabApi) (Proxy @UserApi) "juliendehos"

toRemote :: URI -> URI
toRemote uri = uri
    { uriScheme = "https:"
    , uriAuthority = Just (URIAuth "" "gitlab.com/" "")
    }

