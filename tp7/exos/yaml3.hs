{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import Data.Yaml 

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: Int
    } deriving (Show)

instance FromJSON Person where
    parseJSON = withObject "Person" $ \v -> Person
        <$> v .: "firstname"
        <*> v .: "lastname"
        <*> (read <$> v .: "birthyear")

main :: IO ()
main = do
    -- let res0 = Person "John" "Doe" "1970" False
    --- file <- B.readFile "aeson-test1.jso
    person <- decodeFileEither "yaml-test1.yaml"
    print (person :: Either ParseException Person)

    -- res2 <- decodeFileEither "aeson-test2.json"
    -- print (res2 :: Either String [Person])

    -- res3 <- decodeFileEither "aeson-test3.json"
   -- print (res3 :: Either String [Person])

