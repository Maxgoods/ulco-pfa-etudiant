import qualified Data.Text.IO as T
import qualified Data.Text.Lazy as L
import qualified Data.Text.Lazy.IO as L

main :: IO ()
main = do
    -- lire text1.hs
    file <- T.readFile "text4.hs"
    -- convertir en string
    let contents = L.fromStrict file
    -- afficher
    L.putStrLn contents