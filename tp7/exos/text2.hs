import qualified Data.Text as C
import qualified Data.Text.IO as C


main :: IO ()
main = do
    -- lire text1.hs en 
    file <- C.readFile "text1.hs"
    -- convertir en string
    let contents = C.unpack file
    -- afficher
    putStrLn contents