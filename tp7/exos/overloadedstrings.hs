-- {-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T

newtype Person = Person T.Text deriving Show

persons :: [Person]
persons = [Person (T.pack "John"), Person (T.pack "Haskell")]

main :: IO ()
main = print persons

