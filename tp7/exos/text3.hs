import qualified Data.ByteString.Char8 as C
import qualified Data.Text.Encoding as E
import qualified Data.Text.IO as T

main :: IO ()
main = do
    -- lire text1.hs
    file <- C.readFile "text1.hs"
    -- convertir en string
    let contents = E.decodeUtf8 file
    -- afficher
    T.putStrLn contents