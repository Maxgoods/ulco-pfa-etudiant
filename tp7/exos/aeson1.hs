{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import qualified Data.Text as T
import GHC.Generics
import Data.Aeson 

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: T.Text
    , speakenglish :: Bool
    } deriving (Generic, Show)

instance FromJSON Person

main :: IO ()
main = do
    -- let res0 = Person "John" "Doe" "1970" False
    --- file <- B.readFile "aeson-test1.json"
    
    person <- eitherDecodeFileStrict "aeson-test1.json"
    print (person :: Either String Person)

    res2 <- eitherDecodeFileStrict "aeson-test2.json"
    print (res2 :: Either String [Person])

    res3 <- eitherDecodeFileStrict "aeson-test3.json"
    print (res3 :: Either String [Person])

