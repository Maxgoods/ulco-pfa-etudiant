{-# LANGUAGE OverloadedStrings #-}

import Miso
import Miso.String hiding (map)

data Model = Model
    { _tasks :: [MisoString]
    , _text :: MisoString
    } deriving (Eq)

data Action
    = ActionAdd
    | ActionClean
    -- | ActionDelete
    | ActionUpdateText MisoString
    | ActionNone
    deriving (Eq)

main :: IO ()
main = startApp App
    { initialAction = ActionNone
    , update        = updateModel
    , view          = viewModel
    , model         = Model  ["task1"] "new task"
    , subs          = []
    , events        = defaultEvents
    , mountPoint    = Nothing
    , logLevel      = Off
    }

updateModel :: Action -> Model -> Effect Action Model
updateModel ActionAdd m = noEff m { _tasks = _text m : _tasks m, _text = ""}
updateModel ActionClean m = noEff m { _tasks = []}
--updateModel ActionDelete m = noEff m { _val = (_val m) - 1 }
updateModel (ActionUpdateText str) m = noEff m { _text = str}
updateModel ActionNone m = noEff m

viewModel :: Model -> View Action
viewModel m =
    div_ []
        [ h1_ [ ] [ text "todo-front" ]
        , input_ [value_ (_text m), onInput ActionUpdateText]
        , button_ [ onClick ActionAdd ] [ text "Add" ]
        , button_ [ onClick ActionClean ] [ text "Clean" ]
        , ul_ [] (map mkLi (_tasks m))
        ]
    where mkLi str = li_ [] [ text str]
