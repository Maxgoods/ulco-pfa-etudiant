{-# LANGUAGE OverloadedStrings #-}

module Movie1 where

import Data.Text 
import Database.SQLite.Simple

dbSelectAllMovies :: Connection -> IO [(Int,Text, Int)] -- doesnt work
dbSelectAllMovies conn = query conn "SELECT * FROM movie"

dbSelectAllMoviesFromPersonId :: Connection -> Int -> IO [[Text]]
dbSelectAllMoviesFromPersonId conn i = 
    query conn "SELECT movie_title \
    \FROM prod \
    \INNER JOIN movie ON prod_movie = movie_id \
    \WHERE prod_person = ?" (Only i)
