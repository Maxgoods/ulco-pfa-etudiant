
{-# LANGUAGE OverloadedStrings #-}

import Web.Scotty
import Database.SQLite.Simple
import Control.Monad.IO.Class (liftIO)
import qualified Data.Text.Lazy as L
import Lucid
import TextShow

import Movie2

main :: IO ()
main = scotty 3000 $
    get "/" $ do
        prods <- liftIO $ withConnection "movie.db" Movie2.dbSelectAllProds
        html $ renderText $ viewPage prods
        --text $ L.fromStrict $ _movie $ head prods

viewPage :: [Prod] -> Html ()
viewPage prods = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            title_ "movie-simple"
        body_ $ do
            h1_ "movie_simple"
            mapM_ viewProd prods

viewProd :: Prod -> Html () 
viewProd (Prod movie year person role) = li_ $ do
    strong_ $ toHtml movie
    " ("
    toHtml $ show year
    "), "
    toHtml $ person
    " ("
    toHtml $ role
    ")"