import Database.SQLite.Simple (open, close)

import Movie1
import Movie2

main :: IO ()
main = do
    conn <- open "movie.db"

    putStrLn "\nMovie1.dbSelectAllMovies"
    res1 <- Movie1.dbSelectAllMovies conn
    mapM_ print res1

    putStrLn "\nMovie2.dbSelectAllProds"
    res2 <- Movie2.dbSelectAllProds conn
    mapM_ print res2

    putStrLn "\nMovie1.dbSelectAllMoviesFromPersonId"
    res3 <- Movie1.dbSelectAllMoviesFromPersonId conn 1
    mapM_ print res3

    close conn

