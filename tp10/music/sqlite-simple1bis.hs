{-# LANGUAGE OverloadedStrings #-}

import Data.Text
import Database.SQLite.Simple

selectAllArtists:: Connection -> IO [(Int,Text)]
selectAllArtist conn = query_ conn "SELECT * FROM artist"

selectAllTitlesWithArtists:: Connection -> IO [(Text,Text)]
selectAllTitlesWithArtists conn = query_ conn 
    "SELECT artist_name, title_name \
    \ FROM title \
    \ INNER JOIN artist ON title_artist = artist_id"


main :: IO ()
main = do
    conn <- open "music.db"

    res1 <- selectAllArtists conn
    print res1

    putStrLn "\nprint res2"
    res2 <- selectAllTitlesWithArtists conn
    print res2

    putStrLn "\nmapM_ print res2"
    mapM_ print res2

    close conn