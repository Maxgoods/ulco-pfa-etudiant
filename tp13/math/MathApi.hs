{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module MathApi where

import Servant 

type Math42 = "quarante-deux" :> Get '[JSON] Int

type MathMult2 = "mul2" :> Capture "x" Int :> Get '[JSON] Int

type MathAdd = "add" :> Capture "x" Int :> Capture "y" Int :> Get '[JSON] Int

