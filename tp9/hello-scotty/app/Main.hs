{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import qualified Data.Text.Lazy as L
import Data.Aeson (ToJSON)
import GHC.Generics
import Lucid
import Network.Wai.Middleware.Static (staticPolicy, addBase)
import Web.Scotty


data Person = Person
    { _name :: L.Text
    , _year :: Int
    } deriving (Generic, Show)

instance ToJSON Person

indexPage :: Html ()
indexPage = do
    doctype_
    html_ $ do
       head_ $ meta_ [charset_ "utf8"]
       body_ $ do
            h1_ "Ma page principale"
            ul_ $ do
                li_ $ a_ [href_ "/route1"] "/route1"
                li_ $ a_ [href_ "/route1/route2"] "/route1/route2"
                li_ $ a_ [href_ "/bob.png"] "/bob.png"
                li_ $ a_ [href_ "/html1"] "/html1"
                li_ $ a_ [href_ "/json1"] "/json1"
                li_ $ a_ [href_ "/add1/5/6"] "/add1"
                li_ $ a_ [href_ "/add2/"] "/add2-1"
                li_ $ a_ [href_ "/add2/?param1=5"] "/add2-2"
                li_ $ a_ [href_ "/add2/?param1=5&param2=6"] "/add2-3"
                img_ [src_ "/bob.png"]

main :: IO ()
main = scotty 3000 $ do
    middleware $ staticPolicy $ addBase "static"
    get "/" $ html $ renderText indexPage
    get "/route1" $ text "/route1"
    get "/route1/route2" $ text "/route1/route2"
    get "/html1" $ html "<h1>html</h1"
    get "/json1" $ json (42::Int)
    get "/json2" $ json (Person "tata" 32)
    get "/html" $ json (Person "tata" 32)
    get "/add1/:param1/:param2" $ do
        num1 <- read <$> param "param1"
        num2 <- read <$> param "param2"
        json (num1+num2 :: Int)
    get "/add2" $ do
        num1 <- read <$> param "param1" `rescue` (\_ -> return "0")
        num2 <- read <$> param "param2" `rescue` (\_ -> return "0")
        json (num1+num2 :: Int)
    get "/index" $ redirect "/"
