{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
module View where

import Model
import Lucid
import Data.Text as T
import Data.Text.Lazy as L

viewIndexPage :: [Rider] -> L.Text
viewIndexPage riders = mkPage "Ma page principale" $ do
    mapM_ mkRider riders

mkRider :: Rider -> Html ()
mkRider r = div_ [class_ "row oneRider"] $ do
    h1_ [class_ "col-md-12"] $ toHtml $ _name r
    mapM_ mkImg (_imgs r)

mkImg :: String -> Html ()
mkImg i = div_ [class_ "col-md-6 oneImg"] $ do
    img_ [class_ "img-responsive", src_ (T.pack i)]
    p_ $ toHtml $ i


bootstrap :: T.Text
bootstrap = "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"

mkPage :: L.Text -> Html () -> L.Text
mkPage myTitle myHtml = renderText $ do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            meta_ [ name_ "viewport"
                  , content_ "width=device-width,initial-scale=1,shrink-to-fit=no"]
            link_ [ rel_ "stylesheet", href_ bootstrap]
            link_ [ rel_ "stylesheet", href_ "styles.css"]
            title_ $ toHtml myTitle
        body_ $ div_ [class_ "container"] myHtml
