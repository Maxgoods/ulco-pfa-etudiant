type MyNum = Either String Double

showMyNum :: MyNum -> String
showMyNum (Left str) = "error: " ++ str
showMyNum (Right n) = "result: " ++ show n

mySqrt :: Double -> MyNum
mySqrt x = if x < 0 
    then Left "sqrt négatif"
else Right (sqrt x)

myLog :: Double -> MyNum
myLog x = if x < 0 
    then Left "log négatif"
else Right (log x)

myMul2 :: Double -> MyNum
myMul2 x = Right ( x * 2)

myNeg :: Double -> MyNum
myNeg x = Right (-x)

{-
myCompute :: MyNum
myCompute = 
    case mySqrt 16 of
        Left err -> Left err
        Right r1 ->
            case myNeg r1 of
                Left err2 -> Left err2
                Right r2 -> 
                    case myMul2 r2 of
                        Left e3 -> Left e3
                        Right r3 -> Right r3
-}

myCompute :: MyNum
myCompute = do
    r1 <- mySqrt 16
    r2 <- myNeg r1
    r3 <- myMul2 r2
    return r3

main :: IO ()
main = do
    putStrLn $ showMyNum myCompute
    print myCompute

    print $ do
        r1 <- mySqrt 16
        r2 <- myNeg r1
        myMul2 r2

    print (mySqrt 16 >>= myNeg >>= myMul2)
