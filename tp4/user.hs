import Data.Char (toUpper)
import Data.List.Split (splitOn)

data User = User
    { _name :: String
    , _email :: String
    } deriving Show

parseUser :: String -> Maybe User
parseUser str =
    case splitOn ";" str of
        [name, email] -> Just (User name email)
        _ -> Nothing

-- upperize :: User -> User

main :: IO ()
main = do
    print $ parseUser "toto;toto@tata.com"
    print $ parseUser "null"

    u <- getLine
    print $ parseUser u

    user <- parseUser <$> getLine
    print user

