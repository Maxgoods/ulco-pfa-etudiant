data Tree a = Leaf | Node a (Tree a) (Tree a)

instance Show a => Show (Tree a) where
    show (Node value g d) = "(" ++ show value ++ show g ++ show d ++ ")"
    show Leaf = "_"

instance Foldable Tree where
    foldMap _ Leaf = mempty
    foldMap f (Node e l r) = foldMap f l `mappend` f e `mappend` foldMap f r

mytree1 :: Tree Int
mytree1 = Node 7 (Node 2 Leaf Leaf)
                (Node 37 (Node 13 Leaf Leaf)
                         (Node 42 Leaf Leaf))

mytree2 :: Tree Int
mytree2 = Node 7 (Node 2 Leaf Leaf)
                (Node 37 (Node 13 Leaf Leaf)
                         (Node 42 Leaf Leaf))

main = do
    print mytree1
    print mytree2
    print $ sum mytree1
    print $ maximum mytree1

